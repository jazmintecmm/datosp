@extends('layouts.app')

@section('content')
    <div class="text-center">
        <h1>Datos Personales</h1>

    </div>

    <div class="container">
        <table class="table table-bordered">
            <thead class="thead-dark">
            <th>ID</th>
            <th>Nombre</th>
            <th>Aepllido materno</th>
            <th>Apellido Paterno</th>
            <th>fecha Nacimiento</th>
            <th>Action</th>
            </thead>
            <tbody>

            @foreach($datos as $dato)
                <tr>
                    <td> {{$dato->id}}</td>
                    <td> {{$dato->nombre}}</td>
                    <td> {{$dato->apellidom}}</td>
                    <td> {{$dato->apellidop}}</td>
                    <td> {{$dato->fechanacimiento}}</td>
                    <td>

                        <a href="{{url('/Datos/'.$dato->id.'/edit')}}" class="btn btn-link">Editar</a>
                        @include('datos.delete',  ['$dato'=> $dato])
                    </td>
                </tr>
            @endforeach()
            </tbody>
        </table>


        <a href="{{url('Datos/create')}}" class="btn btn-primary">Agregar</a>
    </div>


@endsection