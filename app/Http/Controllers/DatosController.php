<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dato;
use Illuminate\Support\Facades\Auth;
use function MongoDB\BSON\toCanonicalExtendedJSON;
use function Sodium\add;

class DatosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = Dato::all();
        //detecta los errores
         //dd($products);
        //exit;
        return view('Datos.index',['datos'=>$datos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('Datos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos= new Dato();
        $datos->nombre=$request->nombre;
        $datos->apellidom=$request->apellidom;
        $datos->apellidop=$request->apellidop;
        $datos->fechanacimiento=$request->fechanacimiento;
       

        if ($datos->save())
        {
            return  redirect( 'Datos');

        }
        else
            return  view( 'Datos.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $dato=Dato::find($id);
        return view( "Datos.edit",["datos"=>$dato]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datos= Dato::find($id);
        $datos->nombre=$request->nombre;
        $datos->apellidom=$request->apellidom;
        $datos->apellidop=$request->apellidop;
        $datos->fechanacimiento=$request->fechanacimiento;

        if ($datos->save())
        {
            return  redirect( 'Datos');

        }
        else
            return  view( 'Datos.edit',['datos'=>$datos]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dato::destroy($id);
        return redirect('Datos');
    }
}
